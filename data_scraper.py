#! /usr/bin/python
# -*- coding: utf-8 -*-

# requirments:
# search criteria
# [Membership - any , Location - London , Country - United Kingdom , Category - Any ]
# data required: company, the email address, telephone number and physical address

import csv
import urllib2
from lxml.html import fromstring
import sys


class CompanyDetailsScraping:

    _url_pattern = 'http://www.apsco.org/directory/Advanced.aspx?streetAddress=london&country=234&page_num={}'

    _xpath = {
        'block': '//div[@class="directory-search-item"]',
        'company': 'span[@class="directory-search-item-title"]',
        'email_address': 'span[@class="directory-search-item-email"]',
        'telephone_number': 'span[@class="directory-search-item-telephone"]',
        'physical_address': 'span[@class="directory-search-item-address"]',
        'website': 'span[@class="directory-search-item-website"]'
    }

    def __init__(self):
        pass

    def main(self):
        url_list = (self._url_pattern.format(n) for n in range(1, 23))
        company_details = [self.get_data(self.crawl(url)) for url in url_list]
        company_details = reduce(lambda x, y: x+y, company_details)

        header = ['company', 'email address', 'telephone number',
            'physical address', 'website']
        with open('recruitment_companies.csv', 'w') as file_write:
            cw = csv.writer(file_write, delimiter=',')
            cw.writerow(header)
            cw.writerows(company_details)

    def crawl(self, url):
        html_content = ''
        try:
            response = urllib2.urlopen(url)
            html_content = response.read()
        except:
            pass
        return html_content

    def parse_data(self, node, ele_xpath):
        value = ''
        try:
            value = node.xpath(ele_xpath)[0].text_content()
        except Exception as e:
            print e
        return value

    def get_data(self, html_content):
	reload(sys)
	sys.setdefaultencoding('utf8')
        doc = fromstring(html_content)
        blocks = doc.xpath(self._xpath['block'])

        parse_key = ['company', 'email_address',
            'telephone_number', 'physical_address', 'website']
        page_data = [map(lambda x: self.parse_data(i, self._xpath[x]), parse_key)
            for i in blocks]
        return page_data

if __name__ == '__main__':
    data_scraper = CompanyDetailsScraping()
    data_scraper.main()
