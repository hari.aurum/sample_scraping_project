from data_scraper import company_details_scraping
from lxml.html import fromstring
import unittest


class TestData:

    def get_test_content(self):
        html_content = ''
        with open("test_page.html", 'r') as f:
            html_content = f.read()
        return html_content


class ParserTestCase(unittest.TestCase):

    def test_parse_data(self):
        test_data = TestData()
        html_content = test_data.get_test_content()
        doc = fromstring(html_content)

        data_scraper = company_details_scraping()
        blocks = doc.xpath(data_scraper._xpath['block'])
        parse_key = ['company', 'email_address',
            'telephone_number', 'physical_address', 'website']
        page_data = map(lambda x: data_scraper.parse_data(blocks[0],
            data_scraper._xpath[x]), parse_key)

        self.assertEqual(
            ['Blue Saffron', 'Email: apsco.team@bluesaffron.com',
            'Telephone: 0844 560 0202 ',
            'Blue Saffron Limited, Suite LM 1.1.1 Building 1-2, The Leathermarket, 11-13 Weston Street, London, SE1 3ER, UK',
            'Website: http://www.bluesaffron.com/Industries/Recruitment/APSCo/']
            , page_data)

if __name__ == '__main__':
    unittest.main()
